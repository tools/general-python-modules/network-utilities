import logging
import struct

class Transport():
	"""
	General transport class for exchanging bytestream data over a socket connection
	Exceptions are expected to be managed in the calling function
	"""
	def __init__(self, sock, debug = None):
		"""
		Transport initialization method

		Parameters:
		-----------
		sock : socket
			The socket connection that is used by the transport		

		debug : str
			Additional debug string for log files (invoking class name)
		"""

		if debug is None:
			self.logger = logging.getLogger("general_logs.net.Transport")
		else:
			self.logger = logging.getLogger("general_logs.net.Transport@%s" % debug)

		self.logger.setLevel(logging.DEBUG)

		self.sock = sock

		self.headerSize = 4

		self.rec = bytes()

	def exchange(self, dataSend):
		"""
		Read after write

		Parameters:
		-----------
		dataSend : bytes
			Payload for sending during exchange
		"""
		self.send(dataSend)
		return self.receive()

	def send(self, dataSend):
		"""
		Send a single data block
		
		Parameters:
		-----------
		dataSend : bytes
			Payload for sending during exchange
		"""
		msg = struct.pack('<I', len(dataSend)) + dataSend
		self.sock.sendall(msg)

	def receive(self):
		"""
		Receives a single data block, including header
		"""
		while True:
			# Catch as many bytes as needed for filling up the header
			if len(self.rec) >= self.headerSize:
				self.recLen = struct.unpack('<I', self.rec[:self.headerSize])

				self.recLen = self.recLen[0]

				self.rec = self.rec[self.headerSize:]
				# Exit, enough data was collected for header
				break
			else:
				# Continue collecting
				dat = self.sock.recv(self.headerSize)

				if not dat:
					self.logger.error("Error in header reception.")
					# Empty data is received if server closes connection without FIN
					raise ConnectionAbortedError
				else:
					self.rec += dat

		if self.recLen:
			while True:
				# Extract payload data, specified by the header length
				if len(self.rec) >= self.recLen:
					data = self.rec[:self.recLen]
					self.rec = self.rec[self.recLen:]

					return (self.recLen, data)
					# Exit, enough data was collected for payload. Append it to the data list.
				else:
					# Continue collecting the remaining data
					dat = self.sock.recv(self.recLen-len(self.rec))

					if not dat:
						self.logger.error("Error in payload reception.")
						# Empty data is received if server closes connection without FIN
						raise ConnectionAbortedError
					else:
						self.rec += dat

		else:
			return (self.recLen, None)
