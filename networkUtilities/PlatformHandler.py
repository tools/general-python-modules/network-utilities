from .__Handler__ import __Handler__
from .Client import Client

import logging

class PlatformHandler(__Handler__):
	"""
	Handles TCP requests and controls a TCP client

	The handler is used as a remote object and runs in an own process
	"""
	def __init__(self, ip, port, timeout = 2, retries = 4, debug = None):
		"""
		Platform handler initialization

		Parameters:
		-----------
		ip : str
			The IP adress to which the client tries to connect
		
		port : int
			The port on which the client tries to connect
		
		timeout : double
			Timeout in seconds for individual request
		
		retries : int
			Number of retries of duration <timeout> for each request
		
		debug : str
			Additional debug string for log files (invoking class name)
		"""

		super().__init__(ip, port, timeout = timeout, retries = retries)

		if debug is None:
			self.logger = logging.getLogger("general_logs.net.PlatformHandler")
		else:
			self.logger = logging.getLogger("general_logs.net.PlatformHandler@%s" % debug)

		self.logger.setLevel(logging.DEBUG)

	def testconn(self):
		"""
		Test connection on an address over TCP
		"""
		exists = False

		if self.endpoint is None:
			# Create a client, if not yet existing on this platform
			self.endpoint = Client(self.ip, self.port)
		try:
			# Let the client connect to the platform. If this fails, the platform is unavailable
			self.endpoint.connect(retries = 0)
		except Exception as e:
			# Connection not successful.
			pass
		else:
			# Connection established!
			exists = True
		finally:
			self.disconnect()
			return exists

	def connect(self):
		"""
		Create a TCP client and connect to the platform
		"""
		if self.endpoint is None:
			# Create a client, if not yet existing on this platform
			self.endpoint = Client(self.ip, self.port)
		try:
			# Let the client connect to the platform. If this fails, the platform is unavailable
			self.endpoint.connect()
		except:
			# Connection not successful :(
			self.disconnect()
			return False

	def communicate(self, dataSend, sections = [('uint32', None)], leaveOpen = False):
		"""
		Connect to a platform, exchange data, and disconnect again
		"""
		self.send(dataSend, leaveOpen = True)
		return self.receive(sections = sections, leaveOpen = leaveOpen)