from .__Handler__ import __Handler__
from .Server import Server

import logging


class TcpServerHandler(__Handler__):
    """
    Creates a TCP server and exposes basic functionality (connect, receive, transmit)
    """

    def __init__(self, ip, port, timeout=1, debug=None):
        """
        TCP Server initialization

        Parameters:
        -----------
        ip : str
                The IP adress on which connections are accepted

        port : int
                The port on which the server exposes its services

        timeout : double
                Timeout in seconds for individual request

        debug : str
                Additional debug string for log files (invoking class name)
        """
        super().__init__(ip, port, timeout=timeout)

        if debug is None:
            self.logger = logging.getLogger(
                "general_logs.net.TcpServerHandler")
        else:
            self.logger = logging.getLogger(
                "general_logs.net.TcpServerHandler@%s" % debug)

        self.logger.setLevel(logging.DEBUG)

        self.logger.info("Remote server started.")

    def connect(self):
        """
        Create a TCP client and connect to the server
        """
        self.logger.debug("Connect to client.")

        if self.endpoint is None:
            self.logger.debug("Create new server.")
            # Create a client, if not yet existing on this platform
            self.endpoint = Server(self.ip, self.port, self.timeout)

        try:
            self.logger.debug("Wait for client connection.")
            # Let the client connect to the client. If this fails, the client is unavailable
            self.endpoint.connect()
            self.logger.debug("Connected to client.")

        except Exception as e:
            # Connection not successful :(
            self.logger.debug("Connection failed with %s." % e)
            self.disconnect()
            raise
