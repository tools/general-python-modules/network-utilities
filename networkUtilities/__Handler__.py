import logging
import struct

from collections import deque

class __Handler__():
	def __init__(self, ip, port, timeout = 1, retries = 0):

		self.logger = logging.getLogger("general_logs.net.__Handler__")
		self.logger.setLevel(logging.DEBUG)

		# The ip address and port that this client connects to
		# If these are changed, a new handler should be created
		self.ip = ip
		self.port = port

		self.timeout = timeout
		self.retries = retries

		# The connection endpoint. Can be a client or a server
		self.endpoint = None

	def connect(self):
		"""
		Connect functionality. This has to be overwritten by the child class
		"""
		raise NotImplementedError

	def disconnect(self):
		"""
		Disconnect endpoint, if it exists
		"""
		if self.endpoint is not None:
			self.logger.debug("Disconnect existing handler.")
			self.endpoint.disconnect()
		else:
			self.logger.debug("No handler available for disconnect.")

	def receive(self, sections = None, leaveOpen = False):
		length = 0
		dataReceive = None

		# If already connected, this should do nothing
		self.connect()

		length, dataReceive = self.endpoint.receive()

		if not leaveOpen:
			self.disconnect()

		if (length > 0) and (dataReceive is not None):
			# This returns the received data
			self.logger.debug("Reception of %d bytes of data." % length)
			return self.unpack(dataReceive, length, sections = sections)
		else:
			return None

	def send(self, dataSend, leaveOpen = False):
		# If already connected, this should do nothing
		self.connect()
		self.endpoint.send(dataSend)

		if not leaveOpen:
			self.disconnect()
		
	def communicate(self, dataSend, sections = None, leaveOpen = False):
		"""
		Connect to a platform, exchange data, and disconnect again
		"""
		self.send(dataSend, leaveOpen = True)
		return self.receive(sections = sections, leaveOpen = leaveOpen)

	def unpack(self, dataPayload, length, sections = None):
		"""
		Unpacks bytes according to specified format
		"""

		self.logger.debug("Unpacking sections")

		if sections is None:
			# Standard return format if not specified
			sections = [("raw", None)]

		lengthRest = length
		dataPayloadRest = dataPayload
		collectedOutput = deque()

		for section in sections:
			sectionFormat, sectionLength = section

			if sectionLength is None:
				sectionLength = lengthRest

			self.logger.debug("Unpack (%s -> %d bytes) of remaing %d bytes." % (sectionFormat, sectionLength, lengthRest))

			if sectionFormat == "raw":
				collectedOutput = dataPayloadRest

			else:
				dataPayloadSlice = dataPayloadRest[:sectionLength]

				if sectionFormat == "double":
					# 64 bit double or multiple fields of doubles
					output = deque(struct.unpack('<%dd' % (sectionLength/8), dataPayloadSlice))

				elif sectionFormat == "float":
					# 32 bit float or multiple fields of floats
					output = deque(struct.unpack('<%df' % (sectionLength/4), dataPayloadSlice))

				elif sectionFormat == "int32":
					# 32 bit signed integer or multiple fields of 32 bit signed integer
					output = deque(struct.unpack('<%di' % (sectionLength/4), dataPayloadSlice))

				elif sectionFormat == "uint32":
					# 32 bit unsigned integer or multiple fields of 32 bit unsigned integer
					output = deque(struct.unpack('<%dI' % (sectionLength/4), dataPayloadSlice))

				elif sectionFormat == "int16":
					# 16 bit signed integer or multiple fields of 16 bit signed integer
					output = deque(struct.unpack('<%dh' % (sectionLength/2), dataPayloadSlice))

				elif sectionFormat == "uint16":
					# 16 bit unsigned integer or multiple fields of 16 bit unsigned integer
					output = deque(struct.unpack('<%dH' % (sectionLength/2), dataPayloadSlice))

				elif sectionFormat == "int8": # Used for ADC data
					# 8 bit signed integer or multiple fields of 8 bit signed integer
					output = deque(struct.unpack('<%db' % (sectionLength), dataPayloadSlice))

				elif sectionFormat == "uint8":
					# 8 bit unsigned integer or multiple fields of 8 bit unsigned integer
					output = deque(struct.unpack('<%dB' % (sectionLength), dataPayloadSlice))
					
				collectedOutput += output

				# Cut off unpacked section and go to next section
				dataPayloadRest = dataPayloadRest[sectionLength:]
				lengthRest -= sectionLength

				self.logger.debug("Remaining: %d of %d" % (lengthRest, length))

				if lengthRest == 0:
					break

		# Return the unpacked output data
		return collectedOutput
