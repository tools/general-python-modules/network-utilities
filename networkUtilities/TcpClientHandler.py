from .__Handler__ import __Handler__
from .Client import Client

import logging


class TcpClientHandler(__Handler__):
    """
    Creates a TCP client and exposes basic functionality (connect, receive, transmit)
    """

    def __init__(self, ip, port, timeout=1, retries=2, debug=None):
        """
        TCP Client initialization

        Parameters:
        -----------
        ip : str
                The IP adress to which the client tries to connect

        port : int
                The port on which the client tries to connect

        timeout : double
                Timeout in seconds for individual request

        retries : int
                Number of retries of duration <timeout> for each request

        debug : str
                Additional debug string for log files (invoking class name)
        """
        super().__init__(ip, port, timeout=timeout, retries=retries)

        if debug is None:
            self.logger = logging.getLogger(
                "general_logs.net.TcpClientHandler")
        else:
            self.logger = logging.getLogger(
                "general_logs.net.TcpClientHandler@%s" % debug)

        self.logger.setLevel(logging.DEBUG)

        self.logger.debug("Remote client started.")

    def connect(self):
        """
        Create a TCP client and connect to the server
        """
        if self.endpoint is None:
            # Create a client
            self.endpoint = Client(
                self.ip, self.port, self.timeout, self.retries)

            self.logger.debug("Created a new client.")

        try:
            # Let the client connect to the server. If this fails, the server is unavailable
            self.logger.debug("Try to connect.")
            self.endpoint.connect()

        except Exception as e:
            # Connection not successful :(
            self.logger.debug("Connection failed with error %s." % e)
            self.disconnect()
            raise

        else:
            self.logger.debug("Connection succeeded.")
