from collections import deque

import concurrent.futures as cf
import logging


class HandlerUnblock():
    """
    Takes a handler and makes it non-blocking by running it in a separate thread. Allows to transmit data on a handler in the background.
    """

    def __init__(self, handler, debug=None, exchange=True):
        """
        Handler unblock initialization method

        Parameters:
        -----------
        handler : Handler object
            A connection handler for TCP/UDP client/server

                debug : str
                        Additional debug string for log files (invoking class name)      
        """

        if debug is None:
            self.logger = logging.getLogger(
                "general_logs.remote.HandlerUnblock")
        else:
            self.logger = logging.getLogger(
                "general_logs.remote.HandlerUnblock@%s" % debug)

        self.logger.setLevel(logging.DEBUG)

        self.logger.debug("Created HandlerUnblock instance.")

        # Communication layer
        self.handler = handler

        # Wait for confirmation message of not
        self.exchange = exchange

        # Thread pool in which threads are started
        self.threadPoolExecutor = cf.ThreadPoolExecutor()

        # List of active futures
        self.pushFuture = None

        self.clear()

    def stop(self):
        """ 
        Disconnect from connection partner
        """
        self.logger.debug("Stop handler and disconnect.")
        self.handler.disconnect()

    def clear(self):
        """
        Clears the queue of requested transmissions
        """
        self.q = deque(maxlen=1)

    def enqueue(self, data):
        """
        Attempt to send data to the tcp server. If busy, drop message. This method does not block.

        Parameters:
        -----------
        data : bytes
            Payload data for transmission
        """
        self.logger.debug("Starting data enqueue...")
        self.q.append(data)

        try:
            if not self.pushFuture.running():
                # If future is not running, send data
                self.push(self.q.pop())
                self.logger.debug("Enqueue data pushed to queue.")
            else:
                pass
                self.logger.debug("Remote server busy.")

        except Exception as e:
            self.push(self.q.pop())
            self.logger.debug("Future failed with %s, send data anyway." % e)

    def transmit(self, data):
        """
        Low level transmission function (blocking), called by push()

        Parameters:
        -----------
        data : bytes
            Payload data for transmission
        """
        self.logger.debug("Transmit data.")

        try:
            self.handler.send(data, leaveOpen=True)

        except Exception as e:
            self.logger.debug("Transmission failed with error %s" % e)

        else:
            if self.exchange:
                try:
                    self.handler.receive(leaveOpen=True)

                except Exception as e:
                    self.logger.debug("Reception failed with error %s" % e)

    def push(self, data):
        """
        Pushes data into the handler object. It will try to send it in a separate thread.

        Parameters:
        -----------
        data : bytes
            Payload data for transmission
        """
        self.logger.debug("Push and send data to remove control TCP server.")
        self.pushFuture = self.threadPoolExecutor.submit(self.transmit, data)
