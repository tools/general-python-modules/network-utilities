import logging
import socket

from .Transport import Transport


class Server():
    """
    Generic TCP server implementation
    """

    def __init__(self, ip, port, timeout=2, debug=None):
        """
        TCP Server initialization

        Parameters:
        -----------
        ip : str
                The IP adress to which the client tries to connect

        port : int
                The port on which the client tries to connect

        timeout : double
                Timeout in seconds for individual request

        debug : str
                Additional debug string for log files (invoking class name)
        """

        if debug is None:
            self.logger = logging.getLogger("general_logs.net.Server")
        else:
            self.logger = logging.getLogger(
                "general_logs.net.Server@%s" % debug)

        self.logger.setLevel(logging.DEBUG)

        self.logger.debug("TCP Server initialized @%s:%d." % (ip, port))

        # Localhost IP for local communication
        self.ip = ip
        self.port = port
        self.timeout = timeout

        self.conn = None
        self.transport = None

        self.connected = False

        self.setup()

    def setup(self):
        """
        Setup the TCP server: Pick a socket, bind, and listen for connections
        """
        self.s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

        # self.s.settimeout(self.timeout)

        self.s.bind((self.ip, self.port))
        self.s.listen()
        self.logger.debug("TCP Server listening.")

    def connect(self):
        """
        Connect to a client by accepting a connection
        In case of failure, ignore the attempt.
        """
        if not self.connected:
            try:
                # Try to accept a connection
                self.conn, self.addr = self.s.accept()

            except Exception as e:
                self.logger.debug("Connection failed with error %s." % e)
                raise

            else:
                # Successful connection from a client
                self.connected = True
                self.logger.info('Connection accepted from %s:%d' % self.addr)
                self.transport = Transport(self.conn)

    def disconnect(self):
        """
        Disconnect from a client. If no client was connected, no operation is taken.
        """
        try:
            if self.conn is not None:
                # Try to disconnect
                self.conn.close()
                self.conn = None

                self.connected = False
        except Exception as e:
            self.logger.debug(
                "Disconnect failed with error %s, not connected." % e)

        else:
            self.logger.debug("Disconnected successfully.")

    def send(self, dataSend):
        """
        Transmit a packet of data

        Parameters:
        -----------
        dataSend : bytes
                The payload data for transmission
        """
        try:
            self.logger.debug("Try sending...")
            self.transport.send(dataSend)

        except TimeoutError:
            self.logger.debug("Timeout error.")
            self.disconnect()

        except ConnectionAbortedError:
            self.logger.debug("Connection aborted.")
            self.disconnect()

        except Exception as e:
            self.logger.debug("Other exception: %s" % e)
            self.disconnect()

        else:
            self.logger.debug("Sending succeeded.")

    def receive(self):
        """
        Receive a packet of data
        """
        try:
            data = self.transport.receive()

        except TimeoutError:
            self.logger.debug("A timeout occured on receiving.")
            self.disconnect()

        except ConnectionAbortedError:
            self.logger.debug("Connection was closed on host side.")
            self.disconnect()

        else:
            self.logger.debug("Data received successfully.")
            return data
