import logging
import socket
import time

from .Transport import Transport


class Client():
    def __init__(self, ip, port, timeout=2, retries=4, debug=None):
        """
        TCP Server initialization

        Parameters:
        -----------
        ip : str
        The IP adress to which the client tries to connect

        port : int
        The port on which the client tries to connect

        timeout : double
        Timeout in seconds for individual request

        retries : int
        Number of retries of duration <timeout> for each request

        debug : str
        Additional debug string for log files (invoking class name)
        """

        if debug is None:
            self.logger = logging.getLogger("general_logs.net.Client")
        else:
            self.logger = logging.getLogger(
                "general_logs.net.Client@%s" % debug)

        self.logger.setLevel(logging.DEBUG)

        self.logger.debug("TCP client started.")

        self.ip = ip
        self.port = port

        self.timeout = timeout
        self.retries = retries

        # The socket object
        self.sock = None

        self.recLen = 0
        self.rec = bytearray()

        # The list of received data items
        self.data = []

        self.connected = False

    def connect(self, retries=None):
        """
        Connects to a TCP server. A number of retries can be specified.

        Parameters:
        -----------
        retries : int
        Number of retries of duration <timeout> for each request. This overrides self.retries, if specified.
        """

        if retries is None:
            retryCount = self.retries
        else:
            retryCount = retries

        self.logger.debug("Connecting with %d retries" % retryCount)

        while True:
            try:
                self.logger.debug("Initiate connection")

                if not self.connected:
                    # Connect
                    self.sock = socket.socket(
                        socket.AF_INET, socket.SOCK_STREAM)
                    self.sock.settimeout(self.timeout)

                    self.sock.connect((self.ip, self.port))

                    self.sock.setsockopt(
                        socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

                    self.transport = Transport(self.sock)

                    self.connected = True

                if self.connected:
                    # End loop in case of no exception and active connection
                    self.logger.debug("Connected to host successfully.")
                    break

            except Exception as e:
                retryCount -= 1
                self.connected = False

                if retryCount < 0:
                    self.logger.debug("Connection failed with error %s." % e)
                    raise

                else:
                    self.logger.debug(
                        "Retry connection, %d remaining" % retryCount)
                    # Sleep for "timeout".
                    time.sleep(self.timeout)

    def disconnect(self):
        """
        Disconnects from TCP server
        """
        try:
            self.connected = False
            self.sock.close()
            self.logger.debug("Connection closed.")
        except Exception as e:
            self.logger.debug(
                "Connection did not exist (%s), closing not required." % e)

    def send(self, dataSend):
        """
        Transmit a packet of data

        Parameters:
        -----------
        data : bytes
        Payload data for transmission
        """
        try:
            self.logger.debug("Begin transmitting data.")
            self.transport.send(dataSend)

        except TimeoutError:
            self.logger.debug("A timeout occured on transmitting.")
            self.disconnect()

        except ConnectionAbortedError:
            self.logger.debug("Connection was closed while transmitting.")
            self.disconnect()

    def receive(self, retries=None):
        """
        Receive data from endpoint. A number of retries can be specified.

        Parameters:
        -----------
        retries : int
                Number of retries of duration <timeout> for each request. This overrides self.retries, if specified.
        """
        if retries is None:
            retryCount = self.retries
        else:
            retryCount = retries

        self.logger.debug("Receive data with %d retries" % retryCount)

        while True:
            try:
                self.logger.debug("Begin receiving data.")
                data = self.transport.receive()

            except ConnectionAbortedError:
                self.logger.debug(
                    "Connection was closed on host side while receiving.")
                self.disconnect()
                break

            except socket.timeout:
                retryCount -= 1
                self.connected = False

                if retryCount < 0:
                    self.logger.debug("Receiving timed out.")
                    break
                else:
                    self.logger.debug(
                        "Retry receive on %s with %d retries." % (self.ip, retryCount))

            else:
                self.logger.debug("Receiving data was successul")
                return data
